
class Container 
  
  def initialize(type)
    @width = "8.5'".to_l
    @height = "8'".to_l
    if type == "20".to_l
      @length = "20'".to_l
    else
      @length = "40'".to_l
    end
    
    this.x = 0
    this.y = 0
    this.z = 0
  end

  def coords(originX, originY, originZ, align)
    wdt2 = @width / 2

    pt0 = Geom::Point3d.new originX - wdt2, originY, originZ
    pt1 = Geom::Point3d.new originX + wdt2, originY, originZ
    pt2 = Geom::Point3d.new originX + wdt2, originY + @length, originZ
    pt3 = Geom::Point3d.new originX - wdt2, originY + @length, originZ

    answer = [pt0, pt1, pt2, pt3]
    return answer
  end
  
  def addContainer(originX, originY, originZ, align)
    this.x = originX
    this.y = originY
    this.z = originZ
    newCoords = coords originX, originY, originZ, align
    puts "addContainer "<> newCoords.to_s
    
    group = Sketchup.active_model.add_group
    newFace = group.entities.add_face newCoords
    newFace.pushpull @height
    
    return group
  end
end

